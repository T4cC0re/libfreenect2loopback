//
// Created by t4cc0re on 6/22/20.
//

#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include "common.h"

void bail(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  vfprintf(stderr, format, argp);
  exit(1);
}

void logf(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  vfprintf(stderr, format, argp);
}