//
// Created by t4cc0re on 6/22/20.
//

#include <cstring>
#include <cmath>
#include "greenscreen.h"

Greenscreen::Greenscreen(libfreenect2::Freenect2Device *dev, float greenscreen_cutoff) {
  this->reg = new libfreenect2::Registration(dev->getIrCameraParams(), dev->getColorCameraParams());
  this->undistorted = new libfreenect2::Frame(512, 424, 4);
  this->registered = new libfreenect2::Frame(512, 424, 4);
  this->bigdepth = new libfreenect2::Frame(1920, 1082, 4);
  this->greenscreen_cutoff = greenscreen_cutoff;
}

Greenscreen::~Greenscreen() {
  delete (reg);
  delete (undistorted);
  delete (registered);
  delete (bigdepth);
}

void Greenscreen::applyToFrame(libfreenect2::Frame *rgb, libfreenect2::Frame *depth) {
  float distance_at_pixel;
  this->reg->apply(rgb, depth, this->undistorted, this->registered, true, this->bigdepth);

  for (int col = 0; col < 1920; col++) {
    for (int row = 0; row < 1080; row++) {
      size_t pixel_offset = ((row * 1920) + col) * rgb->bytes_per_pixel;
      memcpy(&distance_at_pixel, this->bigdepth->data + (pixel_offset) + (1920 * this->bigdepth->bytes_per_pixel), 4);

      if ((col <= 300) || col >= (1920 - 300) ||
          (distance_at_pixel > this->greenscreen_cutoff && !std::isnan(distance_at_pixel) &&
           !std::isinf(distance_at_pixel))) {
        rgb->data[pixel_offset] = 0;
        rgb->data[pixel_offset + 1] = 255;
        rgb->data[pixel_offset + 2] = 0;
        rgb->data[pixel_offset + 3] = 0;
      }
    }
  }
}