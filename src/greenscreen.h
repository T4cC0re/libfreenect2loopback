//
// Created by t4cc0re on 6/22/20.
//

#ifndef LIBFREENECT2LOOPBACK_GREENSCREEN_H
#define LIBFREENECT2LOOPBACK_GREENSCREEN_H

#include <libfreenect2/frame_listener.hpp>
#include <libfreenect2/registration.h>
#include <libfreenect2/libfreenect2.hpp>

class Greenscreen {
public:
  Greenscreen(libfreenect2::Freenect2Device *dev, float greenscreen_cutoff);
  ~Greenscreen();
  void applyToFrame(libfreenect2::Frame *rgb, libfreenect2::Frame *depth);
private:
  libfreenect2::Registration* reg;
  libfreenect2::Frame *undistorted, *registered, *bigdepth;
  float greenscreen_cutoff;
};

#endif //LIBFREENECT2LOOPBACK_GREENSCREEN_H
