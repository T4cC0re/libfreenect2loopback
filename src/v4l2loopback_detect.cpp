//
// Created by t4cc0re on 6/22/20.
//

#include "v4l2loopback_detect.h"
#include "common.h"

#include <filesystem>
#include <fstream>
#include <array>
#include <memory>

static std::shared_ptr<std::filesystem::path> find_match() {
  std::string lookup_dir = "/sys/devices/virtual/video4linux";
//  std::shared_ptr<std::filesystem::path> candidate;
  std::string bridge_dev_name = BRIDGE_DEV_NAME;
  try {
    for (auto &dev : std::filesystem::directory_iterator(lookup_dir)) {
      if (dev.is_directory()) {
        // We have a potential candidate :)
        auto candidate = std::shared_ptr<std::filesystem::path>(new std::filesystem::path(dev.path().string()));
        auto name_file = *candidate / "name";
        logf("Found loopback dev: %s\n", candidate->string().c_str());
        std::ifstream name_stream(name_file.string());
        // length + 1, as we need a trailing null separator.
        std::array<char, BRIDGE_DEV_NAME_LEN + 1> name;
        try {
          name_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
          name_stream.getline(&name[0], BRIDGE_DEV_NAME_LEN + 1);
          name_stream.close();
        } catch (std::ifstream::failure e) {
          logf("Could not get name for dev %s: %s\n", candidate->string().c_str(), e.what());
          continue;
        }
        logf("Real name: %s\n", &name[0]);
        logf("Found loopback dev name: %s\n", &name[0]);
        if (bridge_dev_name.compare(&name[0]) == 0) {
          logf("Match found!: %s\n", &name[0]);
          return candidate;
        }
      }
    }
  } catch (const std::filesystem::filesystem_error &e) {
    logf("Error fetching candidates: %s\n", e.what());
  }
  return std::shared_ptr<std::filesystem::path>();
}

static std::shared_ptr<std::string> device_name_by_virtual_device(std::filesystem::path virt_dev) {
  auto ret = std::shared_ptr<std::string>();

  std::filesystem::path dev_name = virt_dev / "dev";
  std::ifstream dev_stream(dev_name.string());
  // length + 1, as we need a trailing null separator.
  std::array<char, 33> dev_nr{};
  try {
    dev_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    dev_stream.getline(&dev_nr[0], 33);
    dev_stream.close();
  } catch (std::exception e) {
    bail("Could not determine dev\n");
  }
  logf("dev nr: %s\n", &dev_nr[0]);
  std::filesystem::path dev_path = std::filesystem::path("/dev/char");
  dev_path /= &dev_nr[0];

  logf("dev_path: %s\n", dev_path.string().c_str());

  if (std::filesystem::exists(dev_path)) {
    ret = std::shared_ptr<std::string>(new std::string(dev_path.string()));
  }

  return ret;
}

std::shared_ptr<std::string> detect_loopback_dev() {
  std::shared_ptr<std::filesystem::path> virt_dev = find_match();

  if (virt_dev == nullptr) {
    return std::shared_ptr<std::string>();
  } else {
    logf("Got virt_dev: %s\n", virt_dev->string().c_str());
  }

  return device_name_by_virtual_device(*virt_dev);
}