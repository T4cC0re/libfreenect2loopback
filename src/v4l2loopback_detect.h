//
// Created by t4cc0re on 6/22/20.
//

#ifndef LIBFREENECT2LOOPBACK_V4L2LOOPBACK_DETECT_H
#define LIBFREENECT2LOOPBACK_V4L2LOOPBACK_DETECT_H

#include <memory>

#define BRIDGE_DEV_NAME "libFreenect2 bridge"
#define BRIDGE_DEV_NAME_LEN 19

std::shared_ptr <std::string> detect_loopback_dev();

#endif //LIBFREENECT2LOOPBACK_V4L2LOOPBACK_DETECT_H
