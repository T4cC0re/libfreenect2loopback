//
// Created by t4cc0re on 6/21/20.
//

#ifndef LIBFREENECT2LOOPBACK_CONVERT_H
#define LIBFREENECT2LOOPBACK_CONVERT_H
#include <libfreenect2/libfreenect2.hpp>

void setup();
void convert(libfreenect2::Frame *rgb);
void teardown();

#endif //LIBFREENECT2LOOPBACK_CONVERT_H
