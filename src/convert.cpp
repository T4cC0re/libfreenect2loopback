//
// Created by t4cc0re on 6/21/20.
//

#include "convert.h"
#include "v4l2loopback_detect.h"
#include <cstdio>
#include <chrono>
#include "common.h"

extern "C"
{
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

AVCodec *codec;
SwsContext *ctx;
AVCodecContext *c = NULL;
int i, ret, x, y, got_output;
FILE *f;
AVFrame *frame, *yuvFrame;
AVPacket *pkt;

static char *get_error_text(const int error) {
  static char error_buffer[255];
  av_strerror(error, error_buffer, sizeof(error_buffer));
  return error_buffer;
}

static void encode(AVCodecContext *enc_ctx, AVFrame *frame, AVPacket *pkt,
                   FILE *outfile) {
  int ret;
  ret = avcodec_send_frame(enc_ctx, frame);
  if (ret < 0) {
    bail("Error sending a frame for encoding: %d: %s\n", ret, get_error_text(ret));
  }
  while (ret >= 0) {
    ret = avcodec_receive_packet(enc_ctx, pkt);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
      return;
    else if (ret < 0) {
      bail("Error during encoding\n");
    }
    fwrite(pkt->data, 1, pkt->size, outfile);
    av_packet_unref(pkt);
  }
}

void setup() {
  auto target = detect_loopback_dev();
  if (!target) {
    bail("No loopback device found. Have you run `modprobe v4l2loopback exclusive_caps=1 card_label=\"libFreenect2 bridge\"`?\n");
  }
  logf("v4l2loopback device: %s\n", target->c_str());

  codec = avcodec_find_encoder(AV_CODEC_ID_RAWVIDEO);
  logf("codec: %d\n", codec);
  if (!codec) {
    bail("Could not allocate video codec\n");
  }

  c = avcodec_alloc_context3(codec);
  if (!c) {
    bail("Could not allocate video codec context\n");
  }

  // Size is fixed for the RGB stream to 1920x1080
  c->width = 1920;
  c->height = 1080;
  c->pix_fmt = AV_PIX_FMT_YUV420P; // This is the pix_fmt v4l2 expects.
  c->time_base = (AVRational){1, 30};
  c->framerate = (AVRational){30, 1};

  pkt = av_packet_alloc();
  if (!pkt) {
    bail("Could not allocate av packet\n");
  }

  ret = avcodec_open2(c, codec, NULL);

  f = fopen("/proc/self/fd/1", "wb");
//  f = fopen(target->c_str(), "wb");
  if (!f) {
    bail("could not open output\n");
  }

  frame = av_frame_alloc();
  if (!frame) {
    bail("could not allocate frame\n");
  }

  yuvFrame = av_frame_alloc();
  if (!yuvFrame) {
    bail("could not allocate yuvFrame\n");
  }

  frame->width = 1920;
  frame->height = 1080;
  frame->linesize[0] = frame->width * 4;
  // frame->format is set dynamically during convert()

  if (!frame->data) {
    bail("frame does not have buffer allocation\n");
  }
}

int frames = 0;

void convert(libfreenect2::Frame *rgb) {
  auto start = std::chrono::high_resolution_clock::now();
//  logf("rendering frame %d\n", frames);
//  logf("rgb fmt: %d\n", rgb->format);
//  logf("rgb width: %d\n", rgb->width);
//  logf("rgb height: %d\n", rgb->height);
//  logf("rgb bytes_per_pixel: %d\n", rgb->bytes_per_pixel);

  // Color format guarantees either RGBX (5) or BGRX (4). Choose the appropriate libav version.
  frame->format = rgb->format == 5 ? AV_PIX_FMT_RGB0 : AV_PIX_FMT_BGR0;
  frame->data[0] = rgb->data;
  frame->pts = frames;

  if (frames == 0) {
    ctx = sws_getContext(c->width, c->height,
                         (AVPixelFormat) frame->format, c->width, c->height,
                         c->pix_fmt, 0, 0, 0, 0);
    logf("Allocating yuvFrame...\n");
    yuvFrame->format = c->pix_fmt;
    yuvFrame->height = frame->height;
    yuvFrame->width = frame->width;

    if (av_image_alloc(
            yuvFrame->data,
            yuvFrame->linesize,
            yuvFrame->width,
            yuvFrame->height,
            c->pix_fmt,
            32
    ) < 0
            ) {
      bail("failed to allocate yuvFrame buffer\n");
    }
    logf("Allocated yuvFrame...\n");
  }

  sws_scale(ctx, frame->data, frame->linesize, 0, c->height, yuvFrame->data, yuvFrame->linesize);

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
//  logf("scaled frame %d after %d microseconds\n", frames, duration.count());

  encode(c, yuvFrame, pkt, f);

  stop = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

//  logf("rendered frame %d after %d microseconds\n", frames, duration.count());
  frames++;
}

void teardown() {
  // flush the encoder
  encode(c, NULL, pkt, f);
  fclose(f);
  avcodec_free_context(&c);
  av_frame_free(&yuvFrame);
  av_frame_free(&frame);
  av_packet_free(&pkt);
}