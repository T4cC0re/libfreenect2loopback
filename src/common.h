//
// Created by t4cc0re on 6/22/20.
//

#ifndef LIBFREENECT2LOOPBACK_COMMON_H
#define LIBFREENECT2LOOPBACK_COMMON_H

#include <libfreenect2/registration.h>

void bail(const char *format, ...);

void logf(const char *format, ...);

#endif //LIBFREENECT2LOOPBACK_COMMON_H
