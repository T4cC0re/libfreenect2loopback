DEVICE ?= $(shell find /sys/devices/virtual/video4linux -mindepth 1 -maxdepth 1 -type d -exec grep -l 'libFreenect2 bridge' {}/name \; | sed 's/name/dev/g' | xargs cat | xargs -I% echo "/dev/char/%")
SRC = $(shell find src -name '*.cpp')
OBJ = $(subst .cpp,.o,$(subst src/,obj/,$(SRC)))
LDFLAGS = -g
CXXFLAGS=-std=c++17
LDLIBS=-lfreenect2 -lavutil -lavcodec -lswscale

.PHONY: all
all: bridge

bridge: $(OBJ)
	$(CXX) $(LDFLAGS) $(LDLIBS) $^ -o $@

obj/%.o: src/%.cpp
	@mkdir -p obj/
	$(CXX) $(CXXFLAGS) -c $^ -o $@

.PHONY: clean
clean:
	$(RM) ./bridge ./compile_commands.json
	$(RM) -r obj/

#########################
## Convenience targets ##
#########################

.PHONY: compiledb
compiledb: compile_commands.json all

# Generates compile_commands.json for importing into CLion
compile_commands.json: | Makefile $(SRC)
	@compiledb -mnvf make bridge

.PHONY: run
run: bridge
	./bridge -frames 200

.PHONY: proto
proto: bridge
	@test -L "$(DEVICE)" || (echo 'You need lo load a v4l2loopback with the name "libFreenect2 bridge" before running this. `make module` can do this for you.'; exit 1)
	@./bridge cl | ffmpeg -fflags nobuffer -flags low_delay -fflags discardcorrupt -f rawvideo -pix_fmt yuv420p -s 1920x1080 -analyzeduration 0 -i - -f v4l2 -c:v copy $(DEVICE)

.PHONY: module
module:
	sudo modprobe v4l2loopback exclusive_caps=1 card_label="libFreenect2 bridge"
